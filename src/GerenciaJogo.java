import javax.swing.JButton;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class GerenciaJogo {
	char player1Simb, player2Simb, simbTurno;
	boolean fimJogo;
	Random rand;
	ArrayList<Integer> auxPos;
	Tabuleiro tab;
	Menu menu;

	public GerenciaJogo() {
		fimJogo = false;
		tab = new Tabuleiro();
		menu = new Menu();
		player1Simb = menu.escolherSimbolo();
		if (player1Simb == 'X') {
			player2Simb = 'O';
		} else {
			player2Simb = 'X';
		}
		auxPos = new ArrayList<Integer>();
		for (int i = 1; i < 10; i++) {
			auxPos.add(i);
		}
		Collections.sort(auxPos);
		inicializaBotoes();
		rand = new Random();
		int valor = rand.nextInt(2) + 1;
		if (valor == 1) {
			simbTurno = player1Simb;
		} else {
			simbTurno = player2Simb;
		}
	}

	public void inicializaBotoes() {
		for (JButton[] vetBtns : tab.btns) {
			for (final JButton btn : vetBtns) {
				btn.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						if ((simbTurno == player1Simb) && (btn.getText() == "")) {
							aplicaJogada(btn);
							if ((!verificaVitoria()) && (!verificaEmpate())) {
								simbTurno = player2Simb;
							}
						}
					}
				});
			}
		}
	}

	public boolean comparaTextoBotoes(JButton btn1, JButton btn2, JButton btn3) {
		if ((btn1.getText().equals(btn2.getText()))
				&& (btn1.getText().equals(btn3.getText())))
			return true;
		return false;
	}

	public boolean verificaVitoria() {
		if ((tab.btns[0][0].getText() != "")
				&& comparaTextoBotoes(tab.btns[0][0], tab.btns[0][1],
						tab.btns[0][2])) {
			return true;
		} else if ((tab.btns[1][0].getText() != "")
				&& comparaTextoBotoes(tab.btns[1][0], tab.btns[1][1],
						tab.btns[1][2])) {
			return true;
		} else if ((tab.btns[2][0].getText() != "")
				&& comparaTextoBotoes(tab.btns[2][0], tab.btns[2][1],
						tab.btns[2][2])) {
			return true;
		} else if ((tab.btns[0][0].getText() != "")
				&& comparaTextoBotoes(tab.btns[0][0], tab.btns[1][0],
						tab.btns[2][0])) {
			return true;
		} else if ((tab.btns[0][1].getText() != "")
				&& comparaTextoBotoes(tab.btns[0][1], tab.btns[1][1],
						tab.btns[2][1])) {
			return true;
		} else if ((tab.btns[0][2].getText() != "")
				&& comparaTextoBotoes(tab.btns[0][2], tab.btns[1][2],
						tab.btns[2][2])) {
			return true;
		} else if ((tab.btns[0][0].getText() != "")
				&& comparaTextoBotoes(tab.btns[0][0], tab.btns[1][1],
						tab.btns[2][2])) {
			return true;
		} else if ((tab.btns[0][2].getText() != "")
				&& comparaTextoBotoes(tab.btns[0][2], tab.btns[1][1],
						tab.btns[2][0])) {
			return true;
		}
		return false;
	}

	public JButton converteJogada(int pos) {
		if (pos < 4) {
			return (tab.btns[0][pos - 1]);
		} else if (pos < 7) {
			return (tab.btns[1][pos - 4]);
		} else {
			return (tab.btns[2][pos - 7]);
		}
	}

	public void removePos(int pos) {
		for (int i = 0; i < auxPos.size(); i++) {
			if (auxPos.get(i) == pos) {
				;
				auxPos.remove(i);
				return;
			}
		}
	}

	public void aplicaJogada(String simb, int pos) {
		converteJogada(pos).setText(simb);
	}

	public void aplicaJogada(JButton btn) {
		btn.setText("" + player1Simb);
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (tab.btns[i][j] == btn) {
					int num;
					if (i == 0) {
						num = j + 1;
					} else if (i == 1) {
						num = j + 4;
					} else {
						num = j + 7;
					}
					removePos(num);
					return;
				}
			}
		}
	}

	public boolean verificaEmpate() {
		if (auxPos.size() == 0) {
			return true;
		} else
			return false;
	}

	public void joga() {
	}
}

class NivelHard extends GerenciaJogo {

	public NivelHard() {
		super();
	}

	public int buscaCantos() {
		for (int i = 0; i < super.auxPos.size(); i++) {
			int temp = super.auxPos.get(i);
			if (temp == 1 || temp == 3 || temp == 7 || temp == 9) {
				return temp;
			}
		}
		return -1;
	}

	public void joga() {
		// Jogada ofensiva
		for (int i = 0; i < super.auxPos.size(); i++) {
			super.aplicaJogada("" + player2Simb, super.auxPos.get(i));
			if (super.verificaVitoria()) {
				super.auxPos.remove(i);
				return;
			} else {
				super.aplicaJogada("", super.auxPos.get(i));
			}
		}
		// Jogada defensiva
		for (int i = 0; i < super.auxPos.size(); i++) {
			super.aplicaJogada("" + player1Simb, super.auxPos.get(i));
			if (super.verificaVitoria()) {
				super.aplicaJogada("" + player2Simb, super.auxPos.get(i));
				super.auxPos.remove(i);
				if (!super.verificaEmpate()) {
					simbTurno = player1Simb;
				}
				return;
			} else {
				super.aplicaJogada("", super.auxPos.get(i));
			}
		}
		// Tenta jogar no meio
		if (super.tab.btns[1][1].getText() == "") {
			super.tab.btns[1][1].setText("" + player2Simb);
			super.removePos(5);
			if (!super.verificaEmpate()) {
				simbTurno = player1Simb;
			}
			return;
		}
		// Tenta jogar em algum canto
		int canto = buscaCantos();
		if (canto != -1) {
			super.aplicaJogada("" + player2Simb, canto);
			super.removePos(canto);
			if (!verificaEmpate()) {
				simbTurno = player1Simb;
			}
			return;
		}
		// Joga na posicao disponivel
		if (super.auxPos.size() > 0) {
			super.aplicaJogada("" + player2Simb, super.auxPos.get(0));
			super.auxPos.remove(0);
			if (!verificaEmpate()) {
				simbTurno = player1Simb;
			}
		}
	}
}