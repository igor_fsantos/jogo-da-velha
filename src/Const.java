public class Const {
	public static int SCREEN_WIDTH = 640;
	public static int SCREEN_HEIGHT = 480;

	public static int BTN_WIDTH = (SCREEN_WIDTH * 20) / 100;
	public static int BTN_HEIGHT = (SCREEN_HEIGHT * 20) / 100;

	public static int x = (SCREEN_WIDTH / 2) - (BTN_WIDTH / 2 + BTN_WIDTH);
	public static int y = (SCREEN_HEIGHT / 2) - (BTN_HEIGHT / 2 + BTN_HEIGHT);
}