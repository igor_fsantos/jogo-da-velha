import javax.swing.*;

public class Tabuleiro {
	JFrame janela;
	public JButton[][] btns;

	public Tabuleiro() {
		janela = new JFrame("Jogo da Velha");
		janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		janela.setLayout(null);
		janela.setSize(Const.SCREEN_WIDTH, Const.SCREEN_HEIGHT);
		janela.setResizable(false);
		janela.setVisible(true);
		this.criaBotoes();
	}

	public void criaBotoes() {
		btns = new JButton[3][3];
		int x = Const.x;
		int y = Const.y;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				btns[i][j] = new JButton("");
				btns[i][j].setLayout(null);
				janela.add(btns[i][j]);
				btns[i][j].setBounds(x, y, Const.BTN_WIDTH, Const.BTN_HEIGHT);
				x += Const.BTN_WIDTH;
			}
			x = Const.x;
			y += Const.BTN_HEIGHT;
		}
	}
}