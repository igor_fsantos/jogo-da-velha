public class JogoVelha {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		GerenciaJogo IA = new NivelHard();
		while (!IA.fimJogo) {
			if (IA.simbTurno == IA.player2Simb) {
				IA.joga();
			}
			if (IA.verificaVitoria()) {
				IA.menu.informaVencedor(IA.simbTurno);
				break;
			} else if (IA.verificaEmpate()) {
				IA.menu.informaEmpate();
				break;
			}
		}
	}
}
