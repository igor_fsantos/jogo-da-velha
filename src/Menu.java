import javax.swing.JOptionPane;

public class Menu {
	Object[] simb = { "X", "O" };
	Object[] opc = { "Sim", "Nao" };

	public char escolherSimbolo() {
		int n = JOptionPane.showOptionDialog(null,
				"Com qual simbolo voce deseja jogar?", "Pergunta",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
				simb, simb[0]);
		if (n == 0) {
			return 'X';
		} else {
			return 'O';
		}
	}

	public void informaVencedor(char simbTurno) {
		JOptionPane.showMessageDialog(null, "Vencedor: " + simbTurno);
	}

	public void informaEmpate() {
		JOptionPane.showMessageDialog(null, "Empate!");
	}

	public boolean jogarNovamente() {
		int n = JOptionPane.showOptionDialog(null, "Deseja jogar novamente?",
				"Pergunta", JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE, null, opc, opc[0]);
		if (n == 0) {
			return true;
		} else {
			return false;
		}
	}

}
